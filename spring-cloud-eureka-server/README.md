### /etc/hosts文件中添加： ###
```
127.0.0.1 harry1
127.0.0.1 harry2
```

### 编译工程后启动： ###

```
java -jar spring-cloud-eureka-server.jar --spring.profiles.actice=harry1
java -jar spring-cloud-eureka-server.jar --spring.profiles.active=harry2
```