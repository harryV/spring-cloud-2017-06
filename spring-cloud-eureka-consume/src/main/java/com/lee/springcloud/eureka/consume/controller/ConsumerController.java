package com.lee.springcloud.eureka.consume.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author lizhe
 */
@RestController
public class ConsumerController {

    @Autowired
    private RestTemplate template;

    @RequestMapping(value = "/ribbon-consumer", method = RequestMethod.GET)
    public String helloConsumer() {
        return template.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
    }
}
