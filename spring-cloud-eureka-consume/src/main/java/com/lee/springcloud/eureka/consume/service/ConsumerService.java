package com.lee.springcloud.eureka.consume.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * @author lizhe
 */
@Service
public class ConsumerService {
    private final Logger logger = Logger.getLogger(getClass());
}
