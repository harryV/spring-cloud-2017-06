# Spring Cloud #

spring cloud 学习记录

### spring-cloud-eureka-server ###
* 注册中心
* 高可用注册中心的配置
### spring-cloud-eureka-service ###
* 注册服务的提供者
###spring-cloud-eureka-consume ###
* 服务发现与消费
###spring-cloud-config-server ###
* 分布式的配置中心
###spring-cloud-config-consume ###
* 配置的发现